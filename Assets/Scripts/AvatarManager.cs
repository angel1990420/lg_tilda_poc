﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarManager : MonoBehaviour
{
    public GameObject ARFaceAnchorManager;
    public GameObject[] avatars;


    public void PressButton(int idx)
    {
        HideAvatars();
        avatars[idx].SetActive(true);
        ARFaceAnchorManager.GetComponent<UnityARFaceAnchorManager>().anchorPrefab = avatars[idx];
    }

    public void HideAvatars()
    {
        for(int i=0; i<avatars.Length; i++)
            avatars[i].SetActive(false);
    }
}
