
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using NatCorder.Clocks;
using NatCorder.Inputs;
using NatMic;

public class VideoRecorder : MonoBehaviour, IAudioProcessor
{

#if !UNITY_EDITOR && UNITY_IOS
    [DllImport("__Internal")]
    extern static private void _requestPermission(string gameObject, string callback);

    [DllImport("__Internal")]
    extern static private void _saveVideoToAlbum(string path, string gameObject, string callback);
#endif

    private const int k_VideoWidth = 720;

    [Header("Microphone")]
    public bool recordMicrophone = false;
    public bool recordOnlyMediaSound = false;
    public AudioSource microphoneSource;

    [Header("UIButton")]
    public GameObject btnCaptureStart;
    public GameObject btnCaptureStop;

    private NatCorder.MP4Recorder videoRecorder;
    //private IClock recordingClock;
    private RealtimeClock recordingClock;
    private CameraInput cameraInput;
    //private AudioInput audioInput;

    private string count;

    public Text coutText;
    public GameObject Image;
    private Coroutine CoCount;
    private float Timer;

    private bool isCount = false;

    private IAudioDevice audioDevice;

    void Start()
    {
        RequestPermission();
    }

    void Update(){

        if(isCount)
        {
            Timer += Time.deltaTime;
            int minutes = Mathf.FloorToInt(Timer /60f);
            int seconds = Mathf.FloorToInt(Timer % 60f);
            count = minutes.ToString("00") + ":" + seconds.ToString("00");
            coutText.text = count;
        }
    }

    public void StartRecording()
    {
        btnCaptureStart.SetActive(false);
        btnCaptureStop.SetActive(true);
#if !UNITY_EDITOR
        StartCoroutine(StartRecord());
#endif
        //CoCount = StartCoroutine(Timer());
        coutText.gameObject.SetActive(true);
        Image.SetActive(true);
        Timer = 0;
        isCount = true;
    }

    IEnumerator StartRecord()
    {
        // Create recording configurations
        var height = k_VideoWidth * Screen.height / Screen.width;

        // Create a recording clock for generating timestamps
        recordingClock = new RealtimeClock();

        yield return new WaitForEndOfFrame();

        videoRecorder = new NatCorder.MP4Recorder(
            k_VideoWidth,
            height,
            24,
            recordMicrophone ? AudioSettings.outputSampleRate : 0,
            recordMicrophone ? (int)AudioSettings.speakerMode : 0,
            Saved
        );

        // Create recording inputs
        cameraInput = new CameraInput(videoRecorder, recordingClock, Camera.main);
        if (recordMicrophone)
        {
            //StartMicrophone();
            //audioInput = new AudioInput(videoRecorder, recordingClock, microphoneSource, true);
            audioDevice = AudioDevice.Devices[0];
            audioDevice.StartRecording(AudioSettings.outputSampleRate, (int)AudioSettings.speakerMode, this);
        }
    }

    private void StartMicrophone()
    {
        // Create a microphone clip
        microphoneSource.clip = Microphone.Start(null, true, 1, 44100);
        while (Microphone.GetPosition(null) <= 0) { }
        //while (!(Microphone.GetPosition(null) > 0))
        //{
        //    yield return true;
        //}

        // Play through audio source
        microphoneSource.timeSamples = Microphone.GetPosition(null);
        microphoneSource.loop = true;
        microphoneSource.Play();
    }

    public void StopRecording()
    {
        btnCaptureStart.SetActive(true);
        btnCaptureStop.SetActive(false);
#if !UNITY_EDITOR
        // Stop the microphone if we used it for recording
        if (recordMicrophone)
        {
            //Microphone.End(null);
            //microphoneSource.Stop();

            //audioInput.Dispose();
            audioDevice.StopRecording();
        }
        // Stop the recording
        cameraInput.Dispose();
        videoRecorder.Dispose();
#endif
        //StopCoroutine(CoCount);
        coutText.gameObject.SetActive(false);
        Image.SetActive(false);
        isCount = false;
    }

    // IEnumerator Timer()
    // {
    //     yield return null;
        
    //     float Timer = 0;

    //     while(true)
    //     {
    //         Timer += Time.deltaTime;
    //         int minutes = Mathf.FloorToInt(Timer /60f);
    //         int seconds = Mathf.FloorToInt(Timer % 60f);
    //         count = minutes.ToString("00") + ":" + seconds.ToString("00");
    //         coutText.text = count;
    //     }
    // }



    public void OnSampleBuffer(float[] sampleBuffer, int sampleRate, int channelCount, long timestamp)
    {
        // Send sample buffers directly to the video recorder for recording
        videoRecorder.CommitSamples(sampleBuffer, recordingClock.Timestamp);
    }

    // StopRecording하고 저장 후에 저장된 path와 함께 호출되는 함수
    // Path 변경 불가하므로 저장된 미디어를 원하는 곳으로 복사해주는 Native 코드 필요하며,
    // 각 플랫폼의 Application.persistentDataPath에 저장되는 것 같다.
    // 저장되는 확장자
    // Android  : .? 
    // iOS      : .MOV
    void Saved(string path)
    {
        Debug.Log("Saved recording to: " + path);
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            string filePath = string.Format("{0}", path);
#if !UNITY_EDITOR && UNITY_IOS
            if (Application.platform == RuntimePlatform.IPhonePlayer) {
                _saveVideoToAlbum(filePath, gameObject.name, "");
            }
#endif
        }
    }

    void RequestPermission()
    {
#if !UNITY_EDITOR && UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer) {
            _requestPermission(gameObject.name, "");
        }
#endif
    }


    // void OnGUI()
    // {
    //     if(isCount){
    //         GUI.Label(new Rect((Screen.width / 2) - (100 / 2),(Screen.height / 2) - (20 / 2),100, 20), count);
    //     }
    // }
}
