﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunglassManager : MonoBehaviour
{

    public GameObject Sunglass1;
    public GameObject Sunglass2;
    public GameObject Sunglass3;

    public void PushButton0()
    {
        HideSunglasses();
    }

    public void PushButton1()
    {
        HideSunglasses();
        Sunglass1.active = true;        
    }
    public void PushButton2()
    {
        HideSunglasses();
        Sunglass2.active = true;
    }
    public void PushButton3()
    {
        HideSunglasses();
        Sunglass3.active = true;
    }

    public void HideSunglasses()
    {
        Sunglass1.active = false;
        Sunglass2.active = false;
        Sunglass3.active = false;
    }
    
}
