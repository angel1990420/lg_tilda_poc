﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    public GameObject SunglassButton;
    public GameObject AvatarButton;

    public void ClickSunglassButton()
    {
        SunglassButton.SetActive(!SunglassButton.activeInHierarchy);
    }

    public void ClickAvatarButton()
    {
        AvatarButton.SetActive(!AvatarButton.activeInHierarchy);
    }
}
