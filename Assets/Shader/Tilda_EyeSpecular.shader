﻿Shader "Tilda/EyeSpecular"
{
    Properties
    {
        _Color("Color", Color) = (0,0,0,0)
        _Roughness ("Roughness", Range(0,1)) = 1
        _Metallic ("Metallic", Range(0,1)) = 0
        //_LightOffset ("Light Offset", Vector) = (0,0,0,0)

        //[Header(Reflections)]
        //_SpecularPower ("Specular Power", Range(0.01,10)) = 5
        //_SpecularIntensity ("Specular Intensity", Range(0,10)) = 1
        //_NormalMultiplier("Normal Multiplier", float) = 1

    }

    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 200


        CGPROGRAM


        #include "UnityPBSLighting.cginc"
        #pragma surface surf fakePBREye fullforwardshadows alpha:fade
        //#pragma target 3.0

        half4 _Color;
        half4 _LightOffset;
        half _Roughness, _Metallic;
        half _NormalMultiplier, _SpecularPower, _SpecularIntensity;



        struct Input
        {
            float2 uv_MainTex;
            float3 lightDir;
            float3 viewDir;
        };

        struct SurfaceOutputEyeSpecular
        {
            float3 Albedo;
            float3 Normal;
            float3 Emission;
            float Smoothness;
            float Metallic;
            float Alpha;
        };




        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)




        void surf (Input IN, inout SurfaceOutputEyeSpecular o)
        {

            o.Albedo = _Color.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = (1-_Roughness);
            o.Alpha = _Color.a;
        }


        float4 LightingfakePBREye (SurfaceOutputEyeSpecular s, float3 lightDir, float3 viewDir, float atten)
        {
            float4 c;
            s.Normal = normalize(s.Normal);

            lightDir = normalize(lightDir + _LightOffset );
            viewDir = normalize(viewDir);
            float3 halfDir = normalize( lightDir + viewDir * _NormalMultiplier );
            
            float NdotH = dot(s.Normal, halfDir );

            //Specular
            float SpecularLight = pow(max(0,NdotH) , _SpecularPower * 10) * _SpecularIntensity * s.Smoothness ;
            //fakeGGX Specular
            float fakeGGX = pow(max(0,NdotH) , _SpecularPower * 30 ) * _SpecularIntensity * s.Smoothness * 10;
            

            c.rgb = s.Albedo + SpecularLight + fakeGGX;
            c.a = s.Alpha ;//+ SpecularLight + fakeGGX;
            return c;
        }






        ENDCG
    }
    FallBack "Diffuse"
}