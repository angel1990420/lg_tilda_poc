﻿Shader "Tilda/Hair"
{
    Properties
    {
        
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}


        [Header(Normal)]
        _NormalTex ("Normal Tex",2D) = "bump"{}
        _NormalStrength ("Normal Strength", Range(0,5)) = 1

        [Header(PBR)]
        _Roughness("Roughness", Range(0,1)) = 0


        [Header(StrandSpecular)]
        _RotateTangent("Rotate Tangent", vector) = (0,0,0,0)
        _UvCon( "UV Controll", Range(0,1)) = 0
        _SpecShiftTex("Specular Shift Texture", 2D) = "gray"{}


        [Space(10)]
        _LowSpecularPosition("Low Specular Position Offset", Range(-10,10) ) = 0
        _LowSpecularColor ("Low Specular Color", Color) = (1,1,1,1)
        _LowSpecularIntensity("Low Specular Intensity", Range(0.01,3) ) = 0.5
        _LowSpecularExponent("Low Specular Glossiness", Range(0.001,0.1)) = 0.01
        _LowShiftMin("Low Shift Min", float) = -0.9
        _LowShiftMax("Low Shift Max", float) = 0.9
        
        [Space(10)]
        _HighSpecularPosition("High Specular Position Offset", Range(-10,10) ) = 0
        _HighSpecularColor ("High Specular Color", Color) = (1,1,1,1)
        _HighSpecularIntensity("High Specular Intensity", Range(0.01,3) ) = 0.5
        _HighSpecularExponent("High Specular Glossiness", Range(0.0001,0.1)) = 0.01
        _HighShiftMin("High Shift Min", float) = -0.9
        _HighShiftMax("High Shift Max", float) = 0.9
        
        [Header(Fresnel)]

        _fresnelColor ("Fresnel Color", color) = (1,1,1,1)
        _FresnelPower ("Fresnel Power", Range(0.01,10)) = 7
        _FresnelIntensity ("Fresnel Intensity", Range(0,3)) = 1

        _IblExposure( "IBL Exposure", Range(0,5)) = 1.5
        _IblBlurAmount ("IBL Blur Amount", Range (0,30)) = 25
        //_NonMetalIBLInfluence ("Non Metal IBl Influence", Range(0, 0.1)) = 0.05
        




        [Header(Rim Light)]
        [Toggle]_RimLightOn ("RimLight On", float) = 0
        [HDR]_RimColor ("RimLight Color", color) = (1,1,1,1)
        _RimPower ("RimLight Power", float) = 1
        _RimIntensity ("RimLight Intensity", float) = 1


        
        [Space(20)]
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull Mode", float) = 2

        _Cutout ("Cut Out", Range(0,1)) = 0.5


    }
    SubShader
    {Tags { "RenderType"="Opaque" }
        LOD 200
        Zwrite On
        ColorMask 0
        Cull [_Cull]    //이거 변수화하기


        CGPROGRAM
        #include "UnityCG.cginc"
        #pragma surface surf fakePBR vertex:vert alphatest:_Cutout addshadow
        //#pragma target 3.0


        sampler2D _MainTex;
        sampler2D _NormalTex;
        sampler2D _MaskTex;
        half _Roughness;
        half _Metallic;
        //half _Cutout;
        half _NormalStrength;
        half _RimPower, _RimIntensity, _RimLightOn;
        half _SpecularPower, _SpecularIntensity, _FresnelPower, _FresnelIntensity, _DiffuseAmount;
        half _IblExposure, _IblIntensity, _IblBlurAmount;
        half _NonMetalIBLInfluence;
        half4 _Color;
        half4 _EmissiveColor;
        half4 _RimColor;
        half4 _fresnelColor;



        struct Input
        {
            float2 uv_MainTex;
            float2 uv_SpecShiftTex;
            //float3 lightDir;
            //float3 viewDir;
            float4 tangent;
            float3 worldPos;
            float3 worldRefl;
            INTERNAL_DATA
        };


        struct SurfaceOutputfakePBR
        {
            float3 Albedo;
            float3 Normal;
            float3 Emission;
            float Alpha;


        };


        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)


        void vert (inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            v.vertex.xyz = v.vertex.xyz;
            o.tangent = float4(UnityObjectToWorldNormal(v.tangent.xyz),1);  //탄젠트도 벡터니까 월드노멀돌리는 행렬로 해 주어야 한다.
        }


        void surf (Input IN, inout SurfaceOutputfakePBR o)
        {
            half4 Albedo = tex2D (_MainTex, IN.uv_MainTex) * _Color ;
            half3 UnpackedNormal = normalize(UnpackNormal(tex2D(_NormalTex, IN.uv_MainTex)));
            
            
            o.Normal = UnpackedNormal * float3(_NormalStrength, _NormalStrength, 1);

            o.Albedo = Albedo.rgb  ;
            
            o.Alpha = Albedo.a;
        }


        float4 LightingfakePBR (SurfaceOutputfakePBR s, float3 lightDir, float3 viewDir, float atten)
        {
            float4 c;

            //Diffuse
            float3 DiffuseLight = s.Albedo * _LightColor0.rgb ;


            c.rgb = DiffuseLight * atten ;
            c.a = s.Alpha;
            //clip(c.a - _Cutout) ;
            return c;
        }


        ENDCG


        Tags { "RenderType"="Transparent" "Queue"="Transparent"}
        LOD 200
        Zwrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
        Cull [_Cull]    //이거 변수화하기


        CGPROGRAM
        #include "UnityCG.cginc"
        #pragma surface surf fakePBR vertex:vert alpha:fade 
        #pragma target 3.0


        sampler2D _MainTex;
        sampler2D _NormalTex;
        sampler2D _MaskTex;
        half _Roughness;
        half _Metallic;
        half _Cutout;
        half _NormalStrength;
        half _RimPower, _RimIntensity, _RimLightOn;
        half _SpecularPower, _SpecularIntensity, _FresnelPower, _FresnelIntensity, _DiffuseAmount;
        half _IblExposure, _IblIntensity, _IblBlurAmount;
        half _NonMetalIBLInfluence;
        half4 _Color;
        half4 _EmissiveColor;
        half4 _RimColor;
        half4 _fresnelColor;



        half shiftTex;
        half4 tangent;


        sampler2D _SpecShiftTex;
        float4 _LightOffset;
        half _LowSpecularIntensity;
        half _LowSpecularExponent;
        half4 _LowSpecularColor;
        half4 _HighSpecularColor;
        half _HighSpecularIntensity;
        half _HighSpecularExponent;
        half4 _RotateTangent;
        half _ShapenAmount;
        half _HighShiftMax, _HighShiftMin;
        half _LowShiftMax, _LowShiftMin;
        half _UvCon;


        half _LowSpecularPosition;
        half _HighSpecularPosition;
        half _LowSpecularLength,_HighSpecularLength ;






        struct Input
        {
            float2 uv_MainTex;
            float2 uv_SpecShiftTex;
            float3 lightDir;
            float3 viewDir;
            float4 tangent;
            float3 worldPos;
            float3 worldRefl;
            INTERNAL_DATA
        };


        struct SurfaceOutputfakePBR
        {
            float3 Albedo;
            float3 Normal;
            float3 Emission;
            float Alpha;


        };


        float4 RotateX(float4 localRotation, float angle)
        {
            float angleX = radians(angle);
            float c = cos(angleX);
            float s = sin(angleX);
            float4x4 rotateXMatrix  = float4x4( 1,  0,  0,  0,
                                                0,  c,  -s, 0,
                                                0,  s,  c,  0,
                                                0,  0,  0,  1);
            return mul(localRotation, rotateXMatrix);
        }




        float4 RotateY(float4 localRotation, float angle)
        {
            float angleY = radians(angle);
            float c = cos(angleY);
            float s = sin(angleY);
            float4x4 rotateYMatrix  = float4x4( c,  0,  s,  0,
                                                0,  1,  0,  0,
                                                -s, 0,  c,  0,
                                                0,  0,  0,  1);
            return mul(localRotation, rotateYMatrix);
        }




        float4 RotateZ(float4 localRotation, float angle)
        {
            float angleZ = radians(angle);
            float c = cos(angleZ);
            float s = sin(angleZ);
            float4x4 rotateZMatrix  = float4x4( c,  -s, 0,  0,
                                                s,  c,  0,  0,
                                                0,  0,  1,  0,
                                                0,  0,  0,  1);
            return mul(localRotation, rotateZMatrix);
        }


        float StrandSpecular(float4 T, float3 V, float3 L, float3 N, float shift, float exponent, float strength)
        {
            float3 Tan = RotateZ(RotateY(RotateX(float4(T.xyz,1),_RotateTangent.x), _RotateTangent.y), _RotateTangent.z).xyz;  
            float3 Nor = N.xyz;
            float3 H = normalize(L+V);
            float3 B = -normalize(cross( Tan ,Nor) * T.w);
            B = normalize(B + Nor * shift );


            float dotBH =  dot(B, H);
            float sinBH = sqrt(1.0 - dotBH*dotBH );
            float dirAtten = smoothstep(-1, 0 , dotBH);
            return dirAtten * pow(sinBH, 1 / (exponent)+00001 ) * strength;
        }






        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)


        void vert (inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            v.vertex.xyz = lerp(v.vertex.xyz, float3(v.texcoord.xy,0), _UvCon);
            o.tangent = float4(UnityObjectToWorldNormal(v.tangent.xyz),1);  //탄젠트도 벡터니까 월드노멀돌리는 행렬로 해 주어야 한다.
        }


        void surf (Input IN, inout SurfaceOutputfakePBR o)
        {
            half4 Albedo = tex2D (_MainTex, IN.uv_MainTex) * _Color ;
            half3 UnpackedNormal = normalize(UnpackNormal(tex2D(_NormalTex, IN.uv_MainTex)));
            
            
            o.Normal = UnpackedNormal * float3(_NormalStrength, _NormalStrength, 1);


            o.Albedo = Albedo.rgb  ;
            shiftTex = tex2D(_SpecShiftTex, IN.uv_SpecShiftTex).r;
            tangent = IN.tangent;
            o.Alpha = Albedo.a;
        }


        float4 LightingfakePBR (SurfaceOutputfakePBR s, float3 lightDir, float3 viewDir, float atten)
        {
            float4 c;


            float3 halfDir = normalize( normalize(lightDir) + normalize(viewDir) );
            
            float NdotL = dot(s.Normal, normalize(lightDir) );
            float NdotV = dot(s.Normal, normalize(viewDir) );
            float NdotH = dot(s.Normal, halfDir );;//dot(s.Normal, normalize(reflect(-lightDir, s.Normal) ));
            float fresnelSrc = 1 - max(0,NdotV);


            //Diffuse
            float3 DiffuseLight = s.Albedo * _LightColor0.rgb * max(0,NdotL);


            //Specular
            float SpecShiftTex = shiftTex;
            float LowShift = lerp(_LowShiftMin, _LowShiftMax, SpecShiftTex);
            float HighShift = lerp(_HighShiftMin, _HighShiftMax, SpecShiftTex);


            float3 LowShiftLightPos = normalize(float3(lightDir + float3(0,_LowSpecularPosition,0 ) ) );
            float3 HighShiftLightPos = normalize(float3(lightDir + float3(0,_HighSpecularPosition,0) ));
            float3 lowAnisoSpec = StrandSpecular( tangent , viewDir, LowShiftLightPos, s.Normal, SpecShiftTex * LowShift, _LowSpecularExponent, _LowSpecularIntensity );
            float3 highAnisoSpec = StrandSpecular(tangent , viewDir, HighShiftLightPos, s.Normal, SpecShiftTex * HighShift, _HighSpecularExponent, _HighSpecularIntensity );


            float3 finalAniso = (lowAnisoSpec * _LowSpecularColor.rgb + highAnisoSpec * _HighSpecularColor.rgb ) * max(0,NdotL);

            //CubeMap
            float3 hdrReflection = 1;
            float3 reflectedDir = reflect(-viewDir, s.Normal);
            float4 reflection = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectedDir, _IblBlurAmount * (1 - _Roughness));
            hdrReflection = DecodeHDR(reflection, unity_SpecCube0_HDR) * _IblExposure * (1 - _Roughness);


            //fresnel
            float3 Fresnel = pow( fresnelSrc, _FresnelPower) * _FresnelIntensity * _fresnelColor.rgb;


            //RimLight
            float3 RimLight = pow( fresnelSrc, _RimPower) * _RimIntensity * _RimColor.rgb;




            c.rgb = DiffuseLight * atten + max(0,finalAniso) + Fresnel + lerp(0,RimLight,_RimLightOn) + s.Emission + hdrReflection;
            c.a = s.Alpha;//saturate(s.Albedo.r + s.Albedo.g + s.Albedo.b);
            //clip(c.a - _Cutout) ;
            return c;
        }


        ENDCG
    }
    FallBack "Diffuse"
}