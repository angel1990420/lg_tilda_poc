﻿Shader "Tilda/EyeShader"
{
    Properties
    {

        _MainTex ("Eyeball Tex", 2D) = "white" {}
        _NormalTex ("Normal Tex", 2D) = "bump" {}
        _NormalStrength ("Normal Strength", float) = 1
        //_MaskTex ("Roughness(R) Tex", 2D) = "black" {}
        _Roughness ("Roughness", Range(0,1)) = 1
        _Metallic ("Metallic", Range(0,1)) = 0

        [Header(Stylized Diffuse)]
        _DiffuseIntensity ("Diffuse Intensity", Range(0,2)) = 1
        [Space(10)]
        _ShadowColor ("Shadow Color", Color) = (0,0,0,1)
        _ShadowThreshold ("Shadow Threshold", Range(0,1)) = 0.5
        _ShadowSmooth ("Shadow Smooth", Range(0,0.5)) = 0.25

        [Header(Reflections)]
        _FresnelPower ("Fresnel Power", Range(0.01,10)) = 7
        _FresnelIntensity ("Fresnel Intensity", Range(0,10)) = 5
        _SpecularPower ("Specular Power", Range(0.01,20)) = 5
        _SpecularIntensity ("Specular Intensity", Range(0,20)) = 1
        _IblExposure( "IBL Exposure", Range(0,5)) = 1.5
        _IblBlurAmount ("IBL Blur Amount", Range (0,30)) = 25
        

        [Space(20)]
        [Header(Parallax_Shadow)]
        _ParallaxMap ("Heightmap(R), Shadow(G)", 2D) = "black" {}
        _Parallax ("Parallax", Range(-0.1, 0.1)) = 0
        //_ShadowColor ("Shadow Color", color) = (0,0,0,0)
        _ShadowPosition ("Shadow Position", Range(-0.2,0.2)) = 0


        [Space(20)]
        [Header(Iris_Specular)]
        _IrisLookAt("Iris LookAt", Range(0,1)) = 1
        _EyeCenter ("Eye Center (XY)", Vector) = (0.5,0.5,0,0)
        _EyePosClamp ("Specular Clamp(XY), Specular Offset(ZW) ", vector) = (0.2,0.2,0.0,0.0)
        _Radius ("Specular Radius", Range(-0.5, 0.5)) = 0
        _Hardness ("Specular Hardness", Range(0,0.5)) = 0.25

    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        Cull Off


        CGPROGRAM


        #include "UnityCG.cginc"
        #pragma surface surf fakePBREye fullforwardshadows vertex:vert nolightmap 
        
        sampler2D _MainTex;
        sampler2D _NormalTex;
        sampler2D _ParallaxMap;
        
        half _Roughness, _Metallic;
        half4 _EyePosClamp, _ShadowColor, _EyeCenter;
        half _Parallax, _Radius, _Hardness;
        half _IrisLookAt;
        half _NormalStrength;
        float _ShadowPosition;
        float _ShadowThreshold, _ShadowSmooth;
        half _SpecularPower, _SpecularIntensity, _FresnelPower, _FresnelIntensity, _MetallicDiffuseAmount, _DiffuseIntensity, _GGXPower;
        half _IblExposure, _IblIntensity, _IblBlurAmount;
        float shadow;
        float sphere;
        float temp;



        struct Input
        {
            float2 uv_MainTex;
            float3 viewDir;
            float4 eyeUVOffset;
            
        };

        struct SurfaceOutputfakePBR
        {
            float3 Albedo;
            float4 PureAlbedo;
            float3 Normal;
            float3 Emission;
            float Smoothness;
            float Metallic;
            float Alpha;


        };




        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)





        //float Luminance(float3 color)
        //{
        //    return color.r * 0.29 + color.g * 0.59 + color.b * 0.12;
        //}

        half3 CalculateDiffuse(half3 lightDir, half3 normalWS, half atten)
        {
            half NdotL = dot(normalWS, lightDir);
        
            half halfLambert = NdotL * 0.5 + 0.5;
            
            half smoothShadow = smoothstep ( _ShadowThreshold - _ShadowSmooth, _ShadowThreshold + _ShadowSmooth, halfLambert * atten );
            half3 ShadowColor = lerp(_ShadowColor.rgb, 1, smoothShadow );   

            half3 stylizedDiffuse = _LightColor0.rgb * ShadowColor;
            return stylizedDiffuse;
        }



        void vert (inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            float3 WorldSpacePos = mul(unity_ObjectToWorld, v.vertex);
            o.viewDir = normalize(_WorldSpaceCameraPos.xyz - WorldSpacePos.xyz);
            //o.lightDir = normalize(_WorldSpaceLightPos0.xyz) ;
            float3 localRightVetor = mul(float3(0,0,1), unity_WorldToObject);
            float3 localUpVetor = mul(float3(1,0,0), unity_WorldToObject);
            float3 localViewDir = mul(o.viewDir, unity_WorldToObject);
            float localRightLeft = step(0, v.vertex.x ) ;
            temp = localRightLeft;


            float localXDotIris = dot(localRightVetor, o.viewDir) * lerp(1,-1,localRightLeft);
            float localXDotSpecular = dot(localRightVetor, o.viewDir) ;
            float localYDot = -dot(localUpVetor, o.viewDir) * lerp(1,-1,localRightLeft);


            o.eyeUVOffset.xy = float2(localXDotIris, -localYDot);// * _EyePosClamp.xy;
            o.eyeUVOffset.zw = float2(-localXDotSpecular, localYDot) * _EyePosClamp.zw;
        }


        void surf (Input IN, inout SurfaceOutputfakePBR o)
        {

            sphere = smoothstep(0 + _Hardness,1 - _Hardness, 1 - saturate(distance(float2(_EyeCenter.x ,_EyeCenter.y) + float2(lerp(0.05,0.05,temp),0.05) + float2(clamp(0,_EyePosClamp.x,IN.eyeUVOffset.z),clamp(0,_EyePosClamp.y,IN.eyeUVOffset.w) ) , IN.uv_MainTex) * 2 ) + _Radius);
            
            

            //parallax
            half h = tex2D (_ParallaxMap, IN.uv_MainTex).r ;
            float2 offset = ParallaxOffset (h, _Parallax, IN.viewDir) ;

            half4 c = tex2D (_MainTex, IN.uv_MainTex + offset) ;
            
            shadow = 1-tex2D(_ParallaxMap, IN.uv_MainTex + float2(0,_ShadowPosition) ).g;
            o.Albedo = c.rgb ;
            o.PureAlbedo.rgb = c.rgb;
            o.PureAlbedo.a = Luminance(c.rgb);
            o.Metallic = _Metallic;
            o.Normal = UnpackNormal(tex2D(_NormalTex, IN.uv_MainTex + offset)) * float3(_NormalStrength,_NormalStrength,1);
            o.Smoothness = 1-_Roughness;//(1-tex2D(_MaskTex, IN.uv_MainTex + offset).r) * (1-_Roughness);
            
            o.Alpha = c.a;
        }


        float4 LightingfakePBREye (SurfaceOutputfakePBR s, float3 lightDir, float3 viewDir, float atten)
        {
            float4 c;
            s.Normal = normalize(s.Normal);

            lightDir = normalize(lightDir);
            viewDir = normalize(viewDir);
            float3 halfDir = normalize( lightDir + viewDir );
            
            float NdotL = dot(s.Normal, lightDir );
            float NdotV = dot(s.Normal, viewDir );
            float NdotH = dot(s.Normal, halfDir );
            float fresnelSrc = 1 - max(0,NdotV);


            //Diffuse
            float3 DiffuseLight = saturate(s.Albedo * _DiffuseIntensity) * CalculateDiffuse(lightDir, s.Normal, atten * shadow);
            

            //Specular
            float SpecularLight = pow(max(0,NdotH) , _SpecularPower * 10) * _SpecularIntensity * s.Smoothness ;
            //fakeGGX Specular
            float fakeGGX = pow(max(0,NdotH) , _SpecularPower * 30 ) * _SpecularIntensity * s.Smoothness * 10;
            //CubeMap
            float3 hdrReflection = 1;
            float3 reflectedDir = reflect( -viewDir, s.Normal );
            float4 reflection = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectedDir, _IblBlurAmount * (1 - s.Smoothness) ) ;
            hdrReflection = DecodeHDR(reflection, unity_SpecCube0_HDR) * _IblExposure  * lerp(0.05, 1, s.Metallic ) * lerp(1,s.PureAlbedo,s.Metallic) * s.Smoothness;

            //fresnel
            float3 Fresnel = pow( fresnelSrc, _FresnelPower) * _FresnelIntensity * lerp(2,1,s.Smoothness) * lerp(2,4, s.Metallic) * hdrReflection ;

            float3 combineLight = (SpecularLight + lerp(0,fakeGGX,s.Metallic) ) * lerp(Luminance(_LightColor0.rgb), _LightColor0.rgb * s.PureAlbedo * 15, s.Metallic)   ;
            c.rgb = DiffuseLight + s.Emission + combineLight * lerp(1,0.5,s.PureAlbedo.a) + Fresnel + hdrReflection + sphere;
            
            c.a = 1;
            return c;
        }






        ENDCG
    }
    FallBack "Diffuse"
}