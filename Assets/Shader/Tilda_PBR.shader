﻿Shader "Tilda/PBR"
{
    Properties
    {
        
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB) ", 2D) = "white" {}
        
        _MaskTex ("Roughness(R) Texture", 2D) = "black" {}
        _Roughness ("Roughness", Range(0,1)) = 0
        _Metallic ("Metallic", Range(0,1)) = 0

        [Header(Stylized Diffuse)]
        _DiffuseIntensity ("Diffuse Intensity", Range(0,2)) = 1
        [Space(10)]
        _MedColor("Med Tone Color", Color) = (1,1,1,1)
        _MedThreshold ("Med Tone Threshold", Range(0,1)) = 1
        _MedSmooth ("Med Tone Smooth", Range(0,0.5)) = 0.25
        [Space(10)]
        _ShadowColor ("Shadow Color", Color) = (0,0,0,1)
        _ShadowThreshold ("Shadow Threshold", Range(0,1)) = 0.5
        _ShadowSmooth ("Shadow Smooth", Range(0,0.5)) = 0.25
        [Space(10)]
        _ReflectColor ("Reflect Color", Color) = (0,0,0,0)
        _ReflectThreshold ("Reflect Threshold", Range(0,1)) = 0
        _ReflectSmooth ("Reflect Smooth", Range(0,0.5)) = 0.25
        

        [Header(Reflections)]
        //_MetallicDiffuseAmount("Metallic Diffuse Amount", Range(0,0.1)) = 0.05
        _FresnelPower ("Fresnel Power", Range(0.01,10)) = 7
        _FresnelIntensity ("Fresnel Intensity", Range(0,10)) = 5
        //_GGXPower ("GGX Power", Range(0.01,20)) = 5
        _SpecularPower ("Specular Power", Range(0.01,20)) = 5
        _SpecularIntensity ("Specular Intensity", Range(0,20)) = 1
        _IblExposure( "IBL Exposure", Range(0,5)) = 1.5
        _IblBlurAmount ("IBL Blur Amount", Range (0,30)) = 25
        //_NonMetalIBLInfluence ("Non Metal IBl Influence", Range(0, 0.1)) = 0.05
        
        [Header(Normal)]
        _NormalTex ("Normal Tex",2D) = "bump"{}
        _NormalStrength ("Normal Strength", Range(0,5)) = 1

        

        [Header(Rim Light)]
        [Toggle]_RimLightOn ("RimLight On", float) = 0
        [HDR]_RimColor ("RimLight Color", color) = (1,1,1,1)
        _RimPower ("RimLight Power", float) = 1
        _RimIntensity ("RimLight Intensity", float) = 1


        [Space(20)]
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull Mode", float) = 2


    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        Cull [_Cull]    //이거 변수화하기


        CGPROGRAM
        #include "UnityCG.cginc"
        #pragma surface surf fakePBR fullforwardshadows
        //#pragma target 3.0


        sampler2D _MainTex;
        sampler2D _NormalTex;
        sampler2D _MaskTex;
        half _Roughness;
        half _Metallic;
        half _NormalStrength;
        half _RimPower, _RimIntensity, _RimLightOn;
        half _SpecularPower, _SpecularIntensity, _FresnelPower, _FresnelIntensity, _MetallicDiffuseAmount, _DiffuseIntensity, _GGXPower;
        float _MedThreshold, _MedSmooth, _ShadowThreshold, _ShadowSmooth, _ReflectThreshold, _ReflectSmooth;
        float4 _MedColor, _ShadowColor, _ReflectColor;
        half _IblExposure, _IblIntensity, _IblBlurAmount;
        half _NonMetalIBLInfluence;
        half4 _Color;
        half4 _RimColor;





        struct Input
        {
            float2 uv_MainTex;
            float3 lightDir;
            float3 worldRefl;
            INTERNAL_DATA
        };


        struct SurfaceOutputfakePBR
        {
            float3 Albedo;
            float4 PureAlbedo;
            float3 Normal;
            float3 Emission;
            float Smoothness;
            float Metallic;
            float Alpha;


        };


        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)


        //float Luminance(float3 color)
        //{
        //    return color.r * 0.29 + color.g * 0.59 + color.b * 0.12;
        //}

        half3 CalculateDiffuse(half3 lightDir, half3 normalWS, half atten)
        {
            half NdotL = dot(normalWS, lightDir);
        
            half halfLambert = NdotL * 0.5 + 0.5;
            
            half smoothMedTone = smoothstep( _MedThreshold - _MedSmooth, _MedThreshold + _MedSmooth, halfLambert);
            half3 MedToneColor = lerp(_MedColor.rgb , 1 , smoothMedTone);
            half smoothShadow = smoothstep ( _ShadowThreshold - _ShadowSmooth, _ShadowThreshold + _ShadowSmooth, halfLambert * atten );
            half3 ShadowColor = lerp(_ShadowColor.rgb, MedToneColor, smoothShadow );   
            half smoothReflect = smoothstep( _ReflectThreshold - _ReflectSmooth, _ReflectThreshold + _ReflectSmooth, halfLambert);
            half3 ReflectColor = lerp(_ReflectColor.rgb , ShadowColor , smoothReflect);
            half3 stylizedDiffuse = _LightColor0.rgb * ReflectColor;
            return stylizedDiffuse;
        }


        void surf (Input IN, inout SurfaceOutputfakePBR o)
        {


            
            half4 MaskTex = tex2D(_MaskTex, IN.uv_MainTex);
            half4 AlbedoColor = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            half AlbedoLuminance = Luminance(AlbedoColor.rgb);
            half3 UnpackedNormal = normalize(UnpackNormal(tex2D(_NormalTex, IN.uv_MainTex)));


            
            o.Normal = UnpackedNormal * float3(_NormalStrength, _NormalStrength, 1);
            o.Metallic = MaskTex.g * _Metallic;
            o.Albedo = AlbedoColor.rgb * lerp(1, 0.02 , o.Metallic)  ;
            o.PureAlbedo.rgb = AlbedoColor.rgb;
            o.PureAlbedo.a = AlbedoLuminance;
            o.Smoothness = (1-MaskTex.r) * (1-_Roughness);
            o.Alpha = 1;
        }


        float4 LightingfakePBR (SurfaceOutputfakePBR s, float3 lightDir, float3 viewDir, float atten)
        {
            float4 c;
            s.Normal = normalize(s.Normal);

            lightDir = normalize(lightDir);
            viewDir = normalize(viewDir);
            float3 halfDir = normalize( lightDir + viewDir );
            
            float NdotL = dot(s.Normal, lightDir );
            float NdotV = dot(s.Normal, viewDir );
            float NdotH = dot(s.Normal, halfDir );
            float fresnelSrc = 1 - max(0,NdotV);


            //Diffuse
            float3 DiffuseLight = saturate(s.Albedo * _DiffuseIntensity) * CalculateDiffuse(lightDir, s.Normal, atten);
            


            //Specular
            float SpecularLight = pow(max(0,NdotH) , _SpecularPower * 10) * _SpecularIntensity * s.Smoothness ;
            //fakeGGX Specular
            float fakeGGX = pow(max(0,NdotH) , _SpecularPower * 30 ) * _SpecularIntensity * s.Smoothness * 10;
            //CubeMap
            float3 hdrReflection = 1;
            float3 reflectedDir = reflect( -viewDir, s.Normal );
            float4 reflection = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectedDir, _IblBlurAmount * (1 - s.Smoothness) ) ;
            hdrReflection = DecodeHDR(reflection, unity_SpecCube0_HDR) * _IblExposure  * lerp(0.05, 1, s.Metallic ) * lerp(1,s.PureAlbedo,s.Metallic) * s.Smoothness;


            //fresnel
            float3 Fresnel = pow( fresnelSrc, _FresnelPower) * _FresnelIntensity * lerp(2,1,s.Smoothness) * lerp(2,4, s.Metallic) * hdrReflection ;


            //RimLight
            float3 RimLight = pow( fresnelSrc, _RimPower) * _RimIntensity * _RimColor.rgb;


            float3 combineLight = (SpecularLight + lerp(0,fakeGGX,s.Metallic) ) * lerp(Luminance(_LightColor0.rgb), _LightColor0.rgb * s.PureAlbedo * 15, s.Metallic)   ;
            c.rgb = DiffuseLight + lerp(0,RimLight,_RimLightOn) + s.Emission + combineLight * lerp(1,0.5,s.PureAlbedo.a) + Fresnel + hdrReflection;
            
            c.a = 1;
            return c;
        }


        ENDCG
    }
    FallBack "Diffuse"
}