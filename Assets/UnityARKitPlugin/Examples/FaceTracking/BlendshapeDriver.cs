﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class BlendshapeDriver : MonoBehaviour {

	SkinnedMeshRenderer skinnedMeshRenderer;
	Dictionary<string, float> currentBlendShapes;

	public bool isAddedName = true;

	public string groupName = "FACS_BSN.";


	// Use this for initialization
	void Start () {
		skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer> ();

		if (skinnedMeshRenderer) {
			UnityARSessionNativeInterface.ARFaceAnchorAddedEvent += FaceAdded;
			UnityARSessionNativeInterface.ARFaceAnchorUpdatedEvent += FaceUpdated;
		}
	}

	void FaceAdded (ARFaceAnchor anchorData)
	{
		currentBlendShapes = anchorData.blendShapes;
	}

	void FaceUpdated (ARFaceAnchor anchorData)
	{
		currentBlendShapes = anchorData.blendShapes;
	}


	void Update ()
	{
		if (currentBlendShapes != null) {
			foreach(KeyValuePair<string, float> kvp in currentBlendShapes)
			{
				int blendShapeIndex = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(groupName + kvp.Key);
				if(!isAddedName)
					blendShapeIndex = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(kvp.Key);

				if (blendShapeIndex >= 0 )
					skinnedMeshRenderer.SetBlendShapeWeight(blendShapeIndex, kvp.Value * 100.0f);
            }
        }
	}

    // void OnGUI()
    // {
    //    if (currentBlendShapes != null)
    //    {
    //        int i = 0;

    //        foreach (KeyValuePair<string, float> kvp in currentBlendShapes)
    //        {
    //            int blendShapeIndex = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex("blendShape2." + kvp.Key);
	// 		   if(!isAddedName)
	// 				blendShapeIndex = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(kvp.Key);
    //            if (blendShapeIndex >= 0)
    //                skinnedMeshRenderer.SetBlendShapeWeight(blendShapeIndex, kvp.Value * 100.0f);

    //            string dataInfo = string.Format("<color=black><size=40> Key: {0}, Data: {1}</size></color>", kvp.Key, kvp.Value * 100.0f);
    //            GUI.Label(new Rect(0, i * 45, 512*2, 45), dataInfo);
    //            ++i;

    //        }
    //    }
    // }
}
