#import <Foundation/NSException.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import <UIKit/UIKit.h>

@interface PermissionControllerHelper : NSObject {}

- (void) requestPermission:(NSString *)gameObject withCallback:(NSString *)callback;

@end
