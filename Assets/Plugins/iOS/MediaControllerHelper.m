#import "MediaControllerHelper.h"
#import "UIControllerHelper.h"

@implementation MediaControllerHelper

- (void) saveVideoToAlbum:(NSString *)path unityGameObject:(NSString *)gameObject unityCallback:(NSString *)callback {    
    UISaveVideoAtPathToSavedPhotosAlbum(path, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
}

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {

    // delete tmp file
    NSError *err;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:(__bridge NSString * _Nonnull)(contextInfo) error:&err];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        NSLog(@"saved");
        // callback
        // UnitySendMessage(([gameObject cStringUsingEncoding:NSUTF8StringEncoding]),
        //                 ([callback cStringUsingEncoding:NSUTF8StringEncoding]), "saved");

        UIControllerHelper* uiControllerHelper = [[UIControllerHelper alloc] init];
        [uiControllerHelper showMessage:@"saved!"];
    }
}

@end