#import "UIControllerHelper.h"
#import "UIView+Toast.h"

@implementation UIControllerHelper

- (void) showMessage:(NSString *)message {
	UIViewController* pRootViewController = UnityGetGLViewController();
	[pRootViewController.view makeToast:message
								duration:2.0
								position:CSToastPositionCenter];
}

@end