#import <Foundation/NSException.h>
#import <UIKit/UIKit.h>

@interface MediaControllerHelper : NSObject {}

- (void) saveVideoToAlbum:(NSString *)path unityGameObject:(NSString *)gameObject unityCallback:(NSString *)callback;

@end