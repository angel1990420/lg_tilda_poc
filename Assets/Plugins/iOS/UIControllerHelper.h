#import <Foundation/NSException.h>
#import <UIKit/UIKit.h>

@interface UIControllerHelper : NSObject {}

- (void) showMessage:(NSString *)message;

@end