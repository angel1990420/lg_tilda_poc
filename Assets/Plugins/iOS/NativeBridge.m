#import "PermissionControllerHelper.h"
#import "MediaControllerHelper.h"

extern void _requestPermission(const char* gameObject, const char* callback)
{
    NSString *strGameObject = [[NSString alloc] initWithUTF8String:gameObject];
    NSString *strCallback = [[NSString alloc] initWithUTF8String:callback];
    
	PermissionControllerHelper* permissionProviderHelper = [[PermissionControllerHelper alloc] init];
	[permissionProviderHelper requestPermission:strGameObject withCallback:strCallback];
}

extern void _saveVideoToAlbum(const char* path, const char* gameObject, const char* callback)
{
	NSString *strPath = [[NSString alloc] initWithUTF8String:path];
    NSString *strGameObject = [[NSString alloc] initWithUTF8String:gameObject];
    NSString *strCallback = [[NSString alloc] initWithUTF8String:callback];
    
	MediaControllerHelper* mediaControllerHelper = [[MediaControllerHelper alloc] init];
	[mediaControllerHelper saveVideoToAlbum:strPath unityGameObject:strGameObject unityCallback:strCallback];
}
