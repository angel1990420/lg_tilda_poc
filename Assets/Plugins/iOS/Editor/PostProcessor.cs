﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System;
using System.IO;

public class PostProcessor
{
    [PostProcessBuild]
    public static void OnPostProcessBuild(BuildTarget target, string path)
    {
        if(target != BuildTarget.iOS) { return; }

        string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
        UnityEditor.iOS.Xcode.PBXProject proj = new UnityEditor.iOS.Xcode.PBXProject();
        proj.ReadFromString(File.ReadAllText(projPath));
        proj.AddFrameworkToProject(proj.TargetGuidByName("Unity-iPhone"), "Photos.framework", false);
        File.WriteAllText(projPath, proj.WriteToString());


        string plistPath = Path.Combine(path, "Info.plist");
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        PlistElementDict rootDict = plist.root;
        rootDict.SetString("NSPhotoLibraryUsageDescription", "AR FACE TRACKING");
        rootDict.SetString("NSPhotoLibraryAddUsageDescription", "AR FACE TRACKING");

        File.WriteAllText(plistPath, plist.WriteToString());
    }
}
